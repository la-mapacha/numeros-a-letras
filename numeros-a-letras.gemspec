Gem::Specification.new do |s|
  s.name        = 'numeros-a-letras'
  s.version     = '0.1.0'
  s.date        = '2019-05-31'
  s.summary     = "Generador del literal de números enteros y decimales para facturación"
  s.description = "Generador del literal de números enteros y decimales para facturación"
  s.authors     = ["Elmer Mendoza"]
  s.email       = 'defreddyelmer@gmail.com'
  s.files       = Dir.glob("{lib}/**/**/*") + ["Rakefile"]
  s.homepage    = 'https://gitlab.com/la-mapacha/numeros-a-letras'
  s.license     = 'MIT'
end
