require 'minitest/autorun'
require 'numeros-a-letras'

class NumerosALetrasTest < Minitest::Test
  def test_numeros_a_letras
    literales = [
      ['mil 00/100 bolivianos', 1_000.00],
      ['un 50/100 bolivianos', 1.50],
      ['trescientos cuarenta y seis 99/100 bolivianos', 346.99],
      ['novecientos noventa y nueve millones setecientos ochenta y seis mil quinientos cuarenta y tres 72/100 bolivianos', 999_786_543.72],
    ]
    literales.each do |literal, numero|
      assert_equal literal, numero.to_bolivianos
    end
  end
end
