# Números a letras

Permite generar el literal de los números para las facturas del Servicio de
Impuestos Nacionales - SIN.

## Instalación

Clonar el repositorio e instalar la gema:

```bash
gem build numeros-a-letras.gemspec
gem install ./numeros-a-letras-0.1.0.gem
```

## Modo de uso

```bash
require 'numeros-a-letras'

10_546.80.to_bolivianos

=> "diez mil quinientos cuarenta y seis 80/100 bolivianos"
```

## Test

```bash
rake test
```

```bash
Run options: --seed 41100

# Running:

.

Finished in 0.001007s, 993.0822 runs/s, 3972.3288 assertions/s.
1 runs, 4 assertions, 0 failures, 0 errors, 0 skips
```

## Bibliografía

* https://guides.rubygems.org/make-your-own-gem/
